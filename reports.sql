-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Mar 2023 pada 03.18
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `report`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `reports`
--

CREATE TABLE `reports` (
  `id_report` int(11) NOT NULL,
  `nama` varchar(15) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` char(150) NOT NULL,
  `egency` varchar(150) NOT NULL,
  `system` varchar(150) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `foto` text DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `id_ts` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `reports`
--

INSERT INTO `reports` (`id_report`, `nama`, `phone`, `email`, `egency`, `system`, `deskripsi`, `foto`, `status`, `id_ts`, `created_at`, `updated_at`) VALUES
(1, 'jin', '098652352', 'audgsuyad@gmail.com', 'bantul', 'dfgadsg', 'sdfagsfg', 'seydteh', '', 0, '2023-02-21 04:05:58', '2023-02-21 04:05:58'),
(2, 'member', 'pppp', 'ppp', 'ppp', '3', 'pp', NULL, '', 0, '2023-02-22 23:00:47', '2023-02-22 23:00:47'),
(3, 'member', 'pppp', 'ppp', 'ppp', '3', 'pp', NULL, '', 0, '2023-02-22 23:02:45', '2023-02-22 23:02:45'),
(4, 'tess', 'tess', 'tess', 'tess', '4', 'esssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', 'check_one_icon_155665.png', 'PENDING', NULL, '2023-03-19 18:48:58', '2023-03-19 18:48:58');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id_report`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `reports`
--
ALTER TABLE `reports`
  MODIFY `id_report` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

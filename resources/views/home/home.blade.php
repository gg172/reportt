<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}">
    <title>Report Bug | GI</title>
    <link rel="stylesheet" href="{{ asset('assets/css/style3.css') }}">
    <link href="{{ asset('assets/css/bootstrap.css') }}"  rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body>
    <header>
            <!-- <a href="#" class="logo">Global Intermedia</a> -->
            <img src="{{ asset('assets/img/logo_gi.png') }}" width="150px" height="25px" class="img-fluid" alt="">
            <ul>
                <li><a href="template" class="active">home</a></li>
                <li><a href="#about">about</a></li>
                <li><a href="#">contact</a></li>
            </ul>
    </header>
    <section>
        <img src="{{ asset('assets/img/stars.png') }}"  id="stars" alt="">
        <img src="{{ asset('assets/img/moon.png') }}" class="img-fluid" id="moon" alt="">
        <img src="{{ asset('assets/img/mountains_behind.png') }}" class="img-fluid" id="mountains_behind" alt="">
        <h2 id="text" class="fw-bold">Report Bug</h2>
        <a href="report" id="btn">Report</a>
        <img src="{{ asset('assets/img/mountains_front.png') }}" class="img-fluid" id="mountains_front" alt="">
    </section>
    <div class="sec" id="sec">
        <h4 id="about">Menghadirkan Gagasan Kreatif dan Inovatif</h4>
        <h2>Berorientasi Pada Kepuasan Konsumen</h2>
        <br>
        <br>
        <h3>Sejarah Pendirian</h3>
        <p>Global Intermedia (G-IM) dikenal sebagai De Concept Computer 2000. Didirikan secara resmi pada 1 Maret 2004 setelah lebih dari 2 tahun (sejak tahun 2001) dilakukan perencanaan-perencanaannya. Anggota tim Global Intermedia telah dilatih dengan baik di berbagai perusahaan dan beberapa institusi, untuk menyediakan tim yang solid dan tangguh. Dengan teknologi informasi sebagai bisnis utama, kami menyediakan aplikasi desktop, aplikasi web, presentasi multimedia dan perawatan jaringan komputer.</p>
        <br>
        <br>
        <h4>Jadi Siapakah Global Intermedia?</h4>
        <p>Global Intermedia adalah perusahaan yang secara spesifik bergerak dalam bidang konsultasi, analisis, desain, serta implementasi sistem informasi yang berkedudukan di Yogyakarta.

            Global Intermedia ingin menjadi bagian dari proses pengembangan teknologi informasi di Indonesia dengan menghadirkan berbagai pilihan produk aplikasi yang handal dan didukung dengan desain antarmuka yang dinamis, menarik dan tentunya user friendly (mudah digunakan).

            Global Intermedia merupakan perusahaan yang dibangun bersama individu-individu yang memiliki kompetensi dan berpengalaman belasan tahun dalam bidang teknologi informasi dan komunikasi. Selama 14 tahun perusahaan telah dipercaya menjadi bagian dari pengembangan sistem informasi untuk berbagai institusi kelembagaan dan telah mendapatkan pengakuan secara luas dalam beragam portofolio.

            Global Intermedia senantiasa berusaha untuk menghadirkan ide-ide kreatif dan inovatif dengan tetap berorientasi terhadap kebutuhan dan kepuasan konsumen. Tim kami selalu memberikan berbagai pertimbangan dan berusaha untuk menghadirkan alternatif solusi dari kebutuhan dan permasalahan klien. Tim Global Intermedia berusaha memposisikan diri sebagai mitra sahabat para klien dan senantiasa terbuka serta berkomunikasi secara intensif dengan klien dalam setiap proses kerjasama. Kemudahan, keberhasilan, dan kepuasan klien adalah fokus utama kami.</p>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <script>
        let stars = document.getElementById('stars');
        let moon = document.getElementById('moon');
        let mountains_behind = document.getElementById('mountains_behind');
        let text = document.getElementById('text');
        let btn = document.getElementById('btn');
        let mountains_front = document.getElementById('mountains_front');
        let header = document.querySelector('header');

        window.addEventListener('scroll', function() {
            let value = window.scrollY;
            stars.style.left = value * 0.25 + 'px';
            moon.style.top = value * 0.60 + 'px';
            mountains_behind.style.top = value * 0.50 + 'px';
            mountains_front.style.top = value * 0 + 'px';
            text.style.marginRight = value * 3 + 'px';
            text.style.marginTop = value * 1.5 + 'px';
            btn.style.marginTop = value * 1.5 + 'px';
            header.style.top = value * 0.50 + 'px';
        })
    </script>
</body>

</html>
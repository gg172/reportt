@extends('template.admin')
@section('konten')
<style>
    table, tr, th, td{
        border: 2px solid black;
    }
</style>
<div class="text-center mt-3">
    <h3 class="fw-bold">Employee Data</h3>
</div>
<div class="container">
    <div class="mt-3 mb-3">
        <a class="btn btn-success" href="{{ route('karyawan.tambah') }}">Add</a><br>
    </div>
    <table class="table text-center align-middle">
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Personal Data</th>
            <th>Position</th>
            <th>Action</th>
        </tr>
        @foreach($karyawan as $t)
        <tr>
            <td>{{ $t->id_karyawan }}</td>
            <td>{{ $t->nama }}</td>
            <td>
                <p><b>Full Name : {{ $t->nama_lengkap}}</b></p>
                <p><b>Gender : {{ $t->jenis_kelamin}}</b></p>
                <p><b>Address : {{ $t->alamat}}</b></p>
                <p><b>Phone : {{ $t->telepon}}</b></p>
                <p><b>E-mail : {{ $t->email}}</b></p>
            </td>
            <td>{{ $t->jabatan }}</td>
            <td class="col-4">
                <a class="btn btn-primary" href="{{ route('karyawan.edit', $t->id_karyawan) }}">EDIT</a>
                <a class="btn btn-danger" href="{{ route('karyawan.hapus', $t->id_karyawan) }}">HAPUS</a>
            </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection
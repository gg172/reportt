@extends('template.admin')
@section('konten')
    <style>
        .card{
            width: 50%;
            margin-left: 23%;
            border: 1px solid black;
        }
    </style>
    <div class="card mt-4">
        <div class="card-body">
            <form action="{{ route('karyawan.update') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input style="border: 1px solid black;" type="hidden" name="id_karyawan" value="{{ $data->id_karyawan }}" required class="form-control">
                </div>
                
                <div class="form-group">
                    <label class="mb-1" for=""><b>Name</b></label>
                    <input style="border: 1px solid black;" type="text" name="nama" value="{{ $data->nama }}" required class="form-control">
                </div>

                <div class="form-group">
                    <label class="mb-1" for=""><b>Full Name</b></label>
                    <input style="border: 1px solid black;" type="text" name="nama_lengkap" value="{{ $data->nama_lengkap }}" required class="form-control">
                </div>

                <div class="form-group">
                    <label class="mb-1" for=""><b>Gender</b></label>
                    <input style="border: 1px solid black;" type="text" name="jenis_kelamin" value="{{ $data->jenis_kelamin }}" required class="form-control">
                </div>

                <div class="form-group">
                    <label class="mb-1" for=""><b>Address</b></label>
                    <input style="border: 1px solid black;" type="text" name="alamat" value="{{ $data->alamat }}" required class="form-control">
                </div>

                <div class="form-group">
                    <label class="mb-1" for=""><b>Phone</b></label>
                    <input style="border: 1px solid black;" type="text" name="telepon" value="{{ $data->telepon }}" required class="form-control">
                </div>

                <div class="form-group">
                    <label class="mb-1" for=""><b>E-mail</b></label>
                    <input style="border: 1px solid black;" type="text" name="email" value="{{ $data->email }}" required class="form-control">
                </div>

                <div class="form-group">
                    <label class="mb-1" for=""><b>Position</b></label>
                    <select name="jabatan" id="" class="form-control">
                    <option value="">Select Position</option>
                    @foreach($jabatan as $t)
                        @if($t->id == $data->jabatan)
                        {{$as = 'selected'}}
                        @else
                        {{$as = ''}}
                        @endif
                        <option value="{{ $t->id }}" {{$as}}>{{ $t->jabatan }}</option>
                    @endforeach
                    </select>
                </div>
                
                <div class="d-grid gap-2 mt-3">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

@extends('template.admin')
@section('konten')
    <style>
        .card{
            width: 50%;
            margin-left: 23%;
            border: 1px solid black;
        }
    </style>
    <div class="card mt-4">
        <div class="card-body">
            <form action="{{ route('karyawan.simpan') }}" method="post">
                {{ csrf_field() }}
                
                <div class="form-group">
                        <label class="mb-1" for=""><b>Name</b></label>
                        <input style="border: 1px solid black;" type="text" name="nama" raquired class="form-control">
                </div>

                <div class="form-group">
                        <label class="mb-1" for=""><b>Full Name</b></label>
                        <input style="border: 1px solid black;" type="text" name="nama_lengkap" raquired class="form-control">
                </div>

                <div class="form-group">
                        <label class="mb-1" for=""><b>Gender</b></label>
                        <input style="border: 1px solid black;" type="text" name="jenis_kelamin" raquired class="form-control">
                </div>

                <div class="form-group">
                        <label class="mb-1" for=""><b>Address</b></label>
                        <input style="border: 1px solid black;" type="text" name="alamat" raquired class="form-control">
                </div>

                <div class="form-group">
                        <label class="mb-1" for=""><b>Phone</b></label>
                        <input style="border: 1px solid black;" type="text" name="telepon" raquired class="form-control">
                </div>

                <div class="form-group">
                        <label class="mb-1" for=""><b>E-mail</b></label>
                        <input style="border: 1px solid black;" type="text" name="email" raquired class="form-control">
                </div>

                <div class="form-group">
                        <label class="mb-1" for=""><b>Position</b></label>
                        <select style="border: 1px solid black;" name="jabatan" class="form-select" style="border-radius: 15px;">
                            <option value="">Select Position</option>
                            @foreach($jabatan as $t)
                            <option value="{{ $t->id }}">{{ $t->jabatan }}</option>
                            @endforeach
                        </select>
                </div>

                <div class="d-grid gap-2 mt-3">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                </div>

            </form>
        </div>
    </div>
@stop

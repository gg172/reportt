@extends('template.admin')
@section('konten')
<style>
    table, tr, th, td{
        border: 2px solid black;
    }
</style>
<div class="container mt-3">
    <!-- <div class="mt-3 mb-3">
        <a class="btn btn-success" href="{{ route('jabatan.tambah') }}">Add</a><br>
    </div> -->
    <table class="table text-center align-middle">
        <tr>
            <th>Id Transaksi</th>
            <th>Id Report</th>
            <th>Technical Support</th>
            <th>Programmer</th>
            <th>Manufacture</th>
        </tr>
        @foreach($transaksi as $t)
        <tr>
            <td>{{ $t->id_transaksi }}</td>
            <td>{{ $t->id_report }}</td>
            <td>{{ $t->id_ts }}</td>
            <td>{{ $t->programmer }}</td>
            <td>{{ $t->created_at }}</td>
        </tr>
        @endforeach
    </table>
</div>
@endsection
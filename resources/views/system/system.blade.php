@extends('template.admin')
@section('konten')
<div class="text-center mt-3">
    <h3 class="fw-bold">Data System</h3>
</div>
<div class="global-container">
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
            <div class="">
                 <a class="btn btn-success" href="{{ route('system.tambah') }}">Add System</a><br>
            </div>
                <table class="table text-center mt-2 justify-content-end" style="border: 1px solid black;">
                    <tr>
                        <th>Id System</th>
                        <th>System Name</th>
                        <th>Action</th>
                    </tr>
                    @foreach($system as $s)
                    <tr>
                        <td>{{ $s->id }} </td>
                        <td>{{ $s->system }} </td>
                        <td> 
                            <a class="btn btn-primary" href="{{ route('system.edit', $s->id) }}">Edit</a>
                            <a class="btn btn-danger" href="{{ route('system.hapus', $s->id) }}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
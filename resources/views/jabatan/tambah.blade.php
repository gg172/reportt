@extends('template.admin')
@section('konten')
    <style>
        .card{
            width: 50%;
            margin-top: 20%;
            margin-left: 23%;
            border: 1px solid black;
        }
    </style>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('jabatan.simpan') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group text-center">
                        <label class="mb-1" for=""><b>Position Name</b></label>
                        <input style="border: 1px solid black;" type="text" name="jabatan" raquired class="form-control">
                </div>

                <div class="d-grid gap-2 mt-3">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                </div>

            </form>
        </div>
    </div>
@stop

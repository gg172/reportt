@extends('template.admin')
@section('konten')
<style>
    table, tr, th, td{
        border: 2px solid black;
    }
</style>
<div class="text-center mt-3">
    <h3 class="fw-bold">Position Data</h3>
</div>
<div class="container">
    <div class="mt-3 mb-3">
        <a class="btn btn-success" href="{{ route('jabatan.tambah') }}">Add</a><br>
    </div>
    <table class="table text-center align-middle">
        <tr>
            <th>Id</th>
            <th>Position Name</th>
            <th>Action</th>
        </tr>
        @foreach($jabatan as $t)
        <tr>
            <td>{{ $t->id }}</td>
            <td>{{ $t->jabatan }}</td>
            <td class="col-4">
                <a class="btn btn-primary" href="{{ route('jabatan.edit', $t->id) }}">Edit</a>
                <a class="btn btn-danger" href="{{ route('jabatan.hapus', $t->id) }}">Delete</a>
            </td>
        </tr>
        @endforeach
    </table>
</div>
@endsection
@extends('template.admin')
@section('konten')
    <style>
        .card{
            width: 50%;
            margin-top: 20%;
            margin-left: 23%;
            border: 1px solid black;
        }
    </style>
    <div class="card text-center">
        <div class="card-body">
            <form action="{{ route('jabatan.update') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input style="border: 1px solid black;" type="hidden" name="id" value="{{ $data->id }}" required class="form-control">
                </div>
                
                <div class="form-group">
                    <label class="mb-1" for=""><b>Position Name</b></label>
                    <input style="border: 1px solid black;" type="text" name="jabatan" value="{{ $data->jabatan }}" required class="form-control">
                </div>
                
                <div class="d-grid gap-2 mt-3">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
@stop

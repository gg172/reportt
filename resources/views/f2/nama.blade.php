@extends('template.admin')
@section('konten')
<div class="container" style="margin-top:150px">
        <div class="col-md-7">
            <form action="{{ route('f2.nama',$nama->id_report) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <a href="{{ url('f2', []) }}" style="margin-left: 1150px;" class="btn btn-danger">Back</a>

                <div class="form-group text-center" style="width: 1200px;">
                    <label for="disabledTextInput">
                        <h1 style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;">INFO</h1>
                    </label>
                    <input type="hidden" name="id_report" value="{{ $nama->id_report }}">
            <div class="card" style="background-color: #333333; color: #FFAC42; ;">
                <div class="card-body" style="font-family: Arial, Helvetica, sans-serif;">
                <p>Nama : {{ $nama->nama }}</p>
                <p>Phone : {{ $nama->phone }}</p>
                <p>Email : {{ $nama->email }}</p>
                <p>Egency : {{ $nama->egency }}</p>
                    </div>
                </form>
        </div> 
</div>
@endsection
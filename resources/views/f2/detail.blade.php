@extends('template.admin')
@section('konten')
<style>
        form{
            width: 85%;
            margin-left: 7%;
        }
        .ppp{
            margin-left: 86%;
        }
        .send{
            width: 85%;
            margin-left: 7%; 
        }
</style>
<a href="{{ route('f2') }}" class="ppp btn btn-danger mt-3">Back</a>
<div class="container mt-2">
<form action="{{ route('f2.simpan') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <input style="border: 1px solid black; border-radius: 10px" type="hidden" name="id_report" value="{{ $report->id_report }}" required class="form-control">
                </div>
                <div class="form-group">
                    <input style="border: 1px solid black; border-radius: 10px" type="hidden" name="status" value="PROSES {{ $report->status }}" required class="form-control">
                </div>
                <div style="border: 2px solid black;"> 
                    <div class="form-group text-center">
                        <h2> <b>Description Problem</b> </h2>
                    </div>
                    <div class="card border border-dark" style="margin-left: 2%; width:30%; background: #222;">
                        <h3 class="text-center" style="color: rgba(250, 250, 250, 0.8);">INFO CLIENT</h3>
                        <div class="ps-3" style="color: whitesmoke;">
                        <p>Nama : {{ $report->nama }}</p>
                        <p>Phone : {{ $report->phone }}</p>
                        <p>Email : {{ $report->email }}</p>
                        <p>Egency : {{ $report->egency }}</p>
                        </div>
                    </div>
                    <div class="card mt-2 border-dark">                   
                         <div class="form-group text-center">
                        <p>{{ $report->deskripsi }}</p>
                        <p>System : {{ $report->system }}</p>
                    </div>
                    
                    <div class="form-group text-center">
                        <img class="img-fluid"  src="{{ asset('storage/report/'.$report->id_report.'/'.$report->foto) }}" alt="">
                        <p>Url : {{$report->url}}</p>
                    </div>
                </div>
                <div class="form-group d-flex mt-2">
                    <label class="mb-1 col-4" for=""><b>Send Problem to Programmer</b></label>
                    <select style="border: 2px solid black;" name="data" class="form-select form-select-sm" style="border-radius: 15px;">
                        <option value="">Select Position</option>
                        @foreach($data as $t)
                        <option value="{{ $t->id_karyawan }}">{{ $t->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="mb-1" for=""><b>keterangan</b></label>
                    <input style="border: 1px solid black;" type="text" name="keterangan" value="{{ $report->keterangan }}" required class="form-control">
                </div>
                <div class="mt-2">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                </div>
                </div>
            </form>
</div>
@endsection
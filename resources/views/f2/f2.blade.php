@extends('template.admin')
@section('konten')
<style>
        select{
            margin-top: 30px;
            border: 2px solid black;
            width: 200px;
        }
        table, tr, th, td{
            border: 2px solid black;
        }
        a{
            text-decoration: none;
        }
        .modal-content{
            border-radius: 50px;
        }
        .modal-header{
            text-align: center;
        }
    </style>
    <div class="container">
    {{ csrf_field() }}
    <select name="filter-system"  id="system" class="bg-secondary system">
            <option value="">SYSTEM</option>
        @foreach($system as $t)

            <option value="{{ $t->id }}">{{ $t->system }}</option>

            <option value="{{ $t->system }}">{{ $t->system }}</option>
        @endforeach
    </select>
        <select name="status" id="" class="bg-secondary">
            <option value="">STATUS</option>
            <option value="">Pending</option>
            <option value="">Process</option>
            <option value="">Selesai</option>
            
        </select>
        <div class="row mt-3">
            <div class="col-md-12">
                <table class="table table align-middle" style="border: 1px solid grey;">
                    <tr class="text-center">
                        <th>NO</th>
                        <th>CLIENT</th>
                        <th>SYSTEM</th>
                        <th>PROBLEM</th>
                        <th>OTHER</th>
                        <th>ACTION</th>
                    </tr>
                    @foreach($report as $u)
                    <tr>
                        <td class="text-center">{{ $u->id_report }}</td>
                        <td class="text-center" style="color: black;">
                        {{ $u->nama }}
                        <a hreflang="" href="{{route ('f2.nama', $u->id_report) }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
</svg></a></td>

                        <td class="text-center">{{ $u->system}}</td>
                        <td class="text-center">
                            
                            <a href="{{route ('f2.detail', $u->id_report) }}">
                            
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots" viewBox="0 0 16 16">
  <path d="M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
</svg></a></td>
                        <td>
                            <p><b>DATE :</b></p>
                            <a>{{ $u->created_at }}</a>
                            <p>
                                <b>STATUS :</b>
                            </p>
                            <label class=" btn btn-warning">{{ ($u->status == 1) ? 'Progress' : 'pending' }}</label>
                        </td>
                        <td class="text-center">
                            @if($u->status == 0)
                           <a class="btn btn-secondary text-dark" href="{{ route('f3') }}">SOLVE THE PROBLEM</a>
                           @endif
                        </td>
                    </tr>
                @endforeach
                 
                </table>
                
            </div>
        </div>
    </div>
</div>
    
    <script src="{{ asset('assets/js/jquery.js') }}"></script>
    <script>
        // let table = $('table').DataTable({
        //     dom: 'Bfrtip',
        //     processing: true,
        //     serverSide:  true,
        //     ajax:'/get-posts',
        //     columns: [{
        //         data: "DT_RowIndex",
        //         orderable: false,
        //         searchable: false
        //     },
        //     {
        //         data: "id_report"
        //     },
        //     {
        //         data: "systems.system"
        //     },
        //     {

        //     }
        //     }]
        // })

        
        $(document).ready(function(){

            $('#system').change(function(){
                console.log('Filter')

                });
        });

                // var system = $(this).val();
                // $.ajax({
                //     url:"{{ route('f2') }}",
                //     type:"GET",
                //     data:{'system':system},
                //     success:function(data){
                //         console.log(data);
                //     }
                // });
                
          
       
</script>
@endsection

@extends('template.admin')
@section('konten')

<div class="text-center mt-3">
    <h3 class="fw-bold">Data User</h3>
</div>
<div class="global-container">
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <table class="table" style="border: 1px solid grey;">
                    <tr>
                        <th>Id</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Jabatan</th>
                        <th>Picture</th>
                        <th>Aksi</th>
                    </tr>
                    @foreach($pengguna as $u)
                    <tr>
                        <td>{{ $u->id_user }} </td>
                        <td>{{ $u->nama }} </td>
                        <td>{{ $u->username}} </td>
                        <td>{{ $u->jabatan}} </td>
                        <td><img style="width: 75px;" height="75px" src="{{ asset('storage/pengguna/'.$u->id_user.'/'.$u->foto) }}" alt="no extist"></td>
                        <td>
                            <a class="btn btn-info" href="{{ route('user.edit', $u->id_user) }}">edit</a>
                            <a class="btn btn-danger" href="{{ route('user.hapus', $u->id_user) }}">hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <a class="btn btn-success" href="{{ route('user.tambah') }}">tambah</a>
    </div>
</div>
@endsection
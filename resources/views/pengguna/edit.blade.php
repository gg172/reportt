@extends('template.admin')
@section('konten')

<div class="container mt-5">
    <div class="col-md-7">
        <form action="{{ route('user.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="hidden" name="id_user" value="{{ $data->id_user }}">
                <input type="text" name="nama" class="form-control" id="nama" value="{{ $data->nama }}" placeholder=" masukan nama anda" required=''>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" value="{{ $data->username }}" aria-describedby="" placeholder="masukan username" required=''>
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" value="{{ $data->password }}" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="">jabatan</label>
                <select onchange="test()" name="jabatan" id="jabatan" class="form-control">
                    
                    @foreach($jabatan as $l)
                    @if($l->id == $data->jabatan)
                    {{$as = 'selected'}}
                    @else
                    {{$as = ''}}
                    @endif
                    <option value="{{ $l-> id }} "{{$as}}>{{ $l->jabatan }}</option>
                    @endforeach
                </select>
            </div>
            
            <!-- <div class="form-group">
                    <label for="level_user">Level User</label>
                    <input type="text" name="level_user" class="form-control" id="level_user" value="{{ $data->level_user }}" placeholder="Level_user">
                </div> -->
            <div class="form-group mt-3">
                <label for=""></label>
                <img style="width: 200px;" src="{{ asset('storage/pengguna/'.$data->id_user.'/'.$data->foto) }}" alt="dfs">
                <input type="file" name="filename[]" value="{{ asset('storage/pengguna/'.$data->id_user.'/'.$data->foto) }}" >
            </div>
            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
</div>

@endsection
@extends('template.admin')
@section('konten')
    <div class="container  mt-5">
        <div class="row">
            <div class="ml-4 col-md-3">
                <form action="{{ route('user.simpan') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">nama</label>
                        <input type="text" class="form-control" name="nama" required>
                    </div>
                    <div class="form-group">
                        <label for="">username</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                   
                    <div class="form-group">
                        <label for="">password</label>
                        <input type="text" class="form-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="">jabatan</label>
                        <select onchange="test()" name="jabatan" id="jabatan" class="form-control">
                            <option value="">pilih level</option>
                            
                            @foreach($jabatan as $l)
                            <option value="{{ $l-> id }}">{{ $l-> jabatan }}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- <div class="form-group" id="form-jabatan" style="display: none;">
                        <label for="">Jabatan</label>
                        <select name="jabatan" id="jabatan" class="form-control">

                        </select>
                    </div> -->
                    <div class="form-group">
                        <label for="">foto</label>
                        <input type="file" name="filename[]" id="">
                    </div>


                    <input class="btn btn-info mt-3" type="submit" value="simpan">
                </form>
                <!-- <button onclick="tampil(2)">test</button>
                <input type="text" id="sistem"> -->
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.3.js"></script>
<!-- <script>
    function test(){
        var level_user = $('#level_user').val();
        if(level_user==3){
            $('#form-jabatan').css('display','block')
        }
        else{
            $('#form-jabatan').css('display','none')
        }
        $.ajax({
            url:`{{ route('user.test_ajax') }}`,
            type:`post`,
            dataType:`json`,
            data:{level_user,
                "_token": "{{ csrf_token() }}"
            },
            success:function(data){
                // console.log(data)
                var html = `<option value="">pilih jabatan</option>`
                for(var row of data){
                    html+=`<option value="${row.id}">${row.jabatan}</option> `
                }


                $(`#jabatan`).html(html)
            }
        })
    }
    function tampil(id){
        $.ajax({
            url:`{{ route('user.test_ajax') }}`,
            type:`post`,
            dataType:`json`,
            data:{id,
                "_token": "{{ csrf_token() }}"
            },
            success:function(data){
                // console.log(data)

                $(`#sistem`).val(data[2].system)
            }
        })
    }
</script> -->
@endsection
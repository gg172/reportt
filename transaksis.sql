-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 20 Mar 2023 pada 03.19
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `report`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksis`
--

CREATE TABLE `transaksis` (
  `id_transaksi` int(11) NOT NULL,
  `id_report` int(11) NOT NULL,
  `programmer` int(100) NOT NULL,
  `ts` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `transaksis`
--

INSERT INTO `transaksis` (`id_transaksi`, `id_report`, `programmer`, `ts`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 0, '', '2023-03-13 19:29:55', '2023-03-13 19:29:55'),
(2, 3, 2, 0, '', '2023-03-13 19:42:41', '2023-03-13 19:42:41'),
(3, 3, 3, 0, '', '2023-03-13 21:44:00', '2023-03-13 21:44:00'),
(4, 2, 2, 0, '', '2023-03-13 21:44:56', '2023-03-13 21:44:56'),
(5, 2, 1, 0, '', '2023-03-16 19:32:40', '2023-03-16 19:32:40'),
(6, 2, 1, 0, '', '2023-03-16 19:40:30', '2023-03-16 19:40:30'),
(7, 2, 1, 0, '', '2023-03-16 23:54:50', '2023-03-16 23:54:50'),
(8, 3, 2, 0, '', '2023-03-17 00:39:03', '2023-03-17 00:39:03'),
(9, 2, 1, 0, '', '2023-03-17 01:59:23', '2023-03-17 01:59:23');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jabatan;

class JabatanController extends Controller
{
    public function index(){
        $jabatan = Jabatan::all();
        return view('jabatan.jabatan',['jabatan' => $jabatan]);
    }

    public function tambah(){
        return view('jabatan.tambah');
    }

    public function simpan(Request $request){
        Jabatan::create([
            'jabatan' =>$request->jabatan,
        ]);
        return redirect()->route('jabatan');
    }

    public function edit($id){
        $data = Jabatan::find($id);
        return view('jabatan.edit',['data' => $data]);
    }

    public function update(Request $request){
        $jabatan = Jabatan::find($request->id);
        $jabatan->id =$request->id;
        $jabatan->jabatan =$request->jabatan;
        $jabatan->save();
        return redirect(route('jabatan'));
    }

    public function hapus($id){
        $jabatan = Jabatan::find($id);
        $jabatan->delete();
        return redirect(route('jabatan'));
    }

}

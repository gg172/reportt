<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\Report;
use App\Models\Karyawan;
use App\Models\Jabatan;

class TransaksiController extends Controller
{
    public function index(){
        // $transaksi = Transaksi::all();
        // $karyawan = Karyawan::where('id_karyawan','nama')->get();
        // $transaksi = Transaksi::join('jabatans','jabatans.id','transaksis.programmer')->get();
        $transaksi = Transaksi::all();
        return view('transaksi.transaksi',['transaksi' => $transaksi]);
    }

}

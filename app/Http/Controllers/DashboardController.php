<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Level;

class DashboardController extends Controller
{
    public function index (){
        
        $id_user = auth()->user()->id_user;
        $data = User::join("jabatans","jabatans.id","users.jabatan")->get();
        $data = User::find($id_user);
        // dd($data->username);
        return view('dashboard.dashboard', ['pengguna' => $data]);
    }

    public function home (){
        
        return view('home.home');
    }
    public function template (){
        $id_user = auth()->user()->id_user;
        $data = User::find($id_user);
        return view('template.admin', ['pengguna' => $data]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\System;

class SystemController extends Controller
{
    public function index(){
        $system = system::all();
        return view('system.system',['system' => $system]);
    }

    public function tambah(){
        return view('system.tambah');
    }

    public function simpan(Request $request){
        system::create([
            'system' =>$request->system,
        ]);
        return redirect(route('system'));
    }

    public function edit($id){
        $data = System::find($id);
        return view('system.edit',['data' => $data]);
    }

    public function update(Request $request){
        $system = System::find($request->id);
        $system->id =$request->id;
        $system->system =$request->system;
        $system->save();
        return redirect(route('system'));
    }

    public function hapus($id){
        $system = system::find($id);
        $system->delete();
        return redirect(route('system'));
    }
}

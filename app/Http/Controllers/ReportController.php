<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\System;
use App\Models\Report;
use App\Models\Karyawan;

class ReportController extends Controller
{
    
    public function index(){
        $system = System::all();
        $report = Report::all();
        return view('halaman_depan.report',['system' => $system, 'report' => $report]);
    }
    public function simpan(Request $request)
    {
        // $id_ts = auth()->user()->id_karyawan;
        $id_report = new Report;
        $id_report = $id_report->max('id_report') + 1;
        $foto = null;
        if ($request->hasFile('filename')) {
            $path = public_path() . '/storage/report/' . $id_report;
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            foreach ($request->file('filename') as $image) {
                $name = $image->getClientOriginalName();
                $image->move($path, $name);
                $foto = $name;
            }
        }

        report::create([
            'nama' =>$request->nama,
            'phone' =>$request->phone,
            'email' =>$request->email,
            'egency' =>$request->egency,
            'system' =>$request->system,
            'deskripsi' =>$request->deskripsi,
            'url' =>$request->url,
            // 'status' =>$request->status,
            'foto' => $foto
        ]);
        // dd($request->all());
        return redirect()->route('save');
    }
}

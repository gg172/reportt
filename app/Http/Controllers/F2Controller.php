<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\System;
use App\Models\Karyawan;
use App\Models\Transaksi;

class F2Controller extends Controller
{

    public function index (){
        $report = Report::join('systems','systems.id','reports.system')->get();
        $system = System::all();
        // dd ($user)        
        return view('f2.f2' , ['report' => $report, 'system'=> $system]);
    }

    public function nama($id_report)
    {  
        $nama = Report::where('id_report', $id_report)->first();
        // dd($id_report);
        return view('f2.nama', ['nama' => $nama]);
    }



    // detaill
    public function detail($id_report)
    {
        $report = Report::join('systems','systems.id','reports.system')->get();
        $report = Report::where('id_report', $id_report)->first();
        // $data = Karyawan::join('karyawans','karyawans.nama','transaksis.programer')->get();
        $data = Karyawan::where('jabatan', '3')->get();
        // $data = Karyawan::all();
        return view('f2.detail', ['report' => $report, 'data'=> $data]);
    }

    public function simpan(Request $request){
        // dd($request);
        $id_ts = auth()->user()->id_karyawan;
        // dd($id_ts);
        // $report = Report::where('id_report', 'status')->update(array('status' => 2));
        // $report->status =$request->status;
        // $report->ts =$request->ts;
        // $report->programmer =$request->programmer;
        // $report->keterangan =$request->keterangan;
        // $report->save();
        Transaksi::create([
            'id_report' =>$request->id_report,
            'status' =>$request->status,
            'programmer' =>$request->data,
            'ts' =>$request->data,
            'keterangan' =>$request->keterangan,
        ]);

        return redirect(route('f2'));
    }
}

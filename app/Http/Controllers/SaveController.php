<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;

class SaveController extends Controller
{
    public function index (){
        $data = Report::all();
        return view('halaman_depan.save',['data' => $data]);
    }
}

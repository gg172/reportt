<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;
use App\Models\Jabatan;
use App\Models\User;

class KaryawanController extends Controller
{
    public function index(){
        $karyawan = Karyawan::join('jabatans','jabatans.id','karyawans.jabatan')->get();
        return view('karyawan.karyawan',['karyawan' => $karyawan]);
    }

    public function tambah(){
        $jabatan = Jabatan::all();
        return view('karyawan.tambah',['jabatan' => $jabatan]);
    }

    public function simpan(Request $request){
        Karyawan::create([
            'nama' =>$request->nama,
            'nama_lengkap' =>$request->nama_lengkap,
            'jenis_kelamin' =>$request->jenis_kelamin,
            'alamat' =>$request->alamat,
            'telepon' =>$request->telepon,
            'email' =>$request->email,
            'jabatan' =>$request->jabatan,
        ]);
        return redirect()->route('karyawan');
    }

    public function edit($id_karyawan){
        $data = Karyawan::find($id_karyawan);
        $jabatan = Jabatan::all();
        return view('karyawan.edit',['data' => $data,'jabatan' => $jabatan]);
    }

    public function update(Request $request){
        $karyawan = Karyawan::find($request->id_karyawan);
        $karyawan->id_karyawan =$request->id_karyawan;
        $karyawan->nama =$request->nama;
        $karyawan->nama_lengkap =$request->nama_lengkap;
        $karyawan->jenis_kelamin =$request->jenis_kelamin;
        $karyawan->alamat =$request->alamat;
        $karyawan->telepon =$request->telepon;
        $karyawan->email =$request->email;
        $karyawan->jabatan =$request->jabatan;
        $karyawan->save();
        return redirect(route('karyawan'));
    }

    public function hapus($id_karyawan){
        $karyawan = Karyawan::find($id_karyawan);
        $karyawan->delete();
        return redirect(route('karyawan'));
    }
}

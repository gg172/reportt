<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $tabel = "report";

    protected $primarykey = 'id_report';
    
    protected $fillable = ['nama','phone','email','egency','system','deskripsi','foto','status','url','ts'];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    
    protected $tabel = "transaksi";

    protected $primaryKey = 'id_transaksi';

    protected $fillable = ['id_report','programmer','ts','keterangan','created_at'];
}

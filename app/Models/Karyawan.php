<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory;

    protected $tabel = "karyawan";

    protected $primaryKey = 'id_karyawan';

    protected $fillable = ['nama','nama_lengkap','jenis_kelamin','alamat','telepon','email','jabatan'];
}
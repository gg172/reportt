<?php

use App\Http\Controllers\ExcelCSVController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'App\Http\Controllers\LoginController@index')->name('login');
Route::get('logout', 'App\Http\Controllers\LoginController@logout')->name('logout');
Route::post('proseslogin', 'App\Http\Controllers\LoginController@proseslogin')->name('proseslogin');

Route::get('report', 'App\Http\Controllers\ReportController@index')->name('report');
Route::post('report/simpan', 'App\Http\Controllers\ReportController@simpan')->name('report.simpan');

Route::get('save', 'App\Http\Controllers\saveController@index')->name('save');
Route::get('cekid', 'App\Http\Controllers\CekidController@index')->name('cekid');
Route::get('reportbug', 'App\Http\Controllers\ReportBugController@index')->name('landing_page');

Route::get('template', 'App\Http\Controllers\DashboardController@template')->name('template');
Route::get('/', 'App\Http\Controllers\DashboardController@home')->name('home');



// f3
Route::get('f3', 'App\Http\Controllers\F3Controller@index')->name('f3');
Route::get('user/api', 'App\Http\Controllers\API\PenggunaController@index')->name('user.api');


Route::get('transaksi', 'App\Http\Controllers\TransaksiController@index')->name('transaksi');

Route::group(['middleware' => 'App\Http\Middleware\ceklogin'], function () {

    Route::get('dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');
    Route::get('f2/detail/{id_report}', 'App\Http\Controllers\F2Controller@detail')->name('f2.detail');
    Route::get('f2', 'App\Http\Controllers\F2Controller@index')->name('f2');
    Route::get('f2/nama/{id_report}', 'App\Http\Controllers\F2Controller@nama')->name('f2.nama');
    Route::post('f2/simpan', 'App\Http\Controllers\F2Controller@simpan')->name('f2.simpan');
    
    Route::middleware(['App\Http\Middleware\role:isAdmin'])->group(function () {

        Route::get('user', 'App\Http\Controllers\PenggunaController@index')->name('user');
        Route::get('user/tambah', 'App\Http\Controllers\PenggunaController@tambah')->name('user.tambah');
        Route::post('user/simpan', 'App\Http\Controllers\PenggunaController@simpan')->name('user.simpan');
        Route::post('user/test_ajax', 'App\Http\Controllers\PenggunaController@test_ajax')->name('user.test_ajax');
        Route::post('user/update', 'App\Http\Controllers\PenggunaController@update')->name('user.update');
        Route::get('user/edit/{id}', 'App\Http\Controllers\PenggunaController@edit')->name('user.edit');
        Route::get('user/hapus/{id}', 'App\Http\Controllers\PenggunaController@hapus')->name('user.hapus');

        Route::get('system', 'App\Http\Controllers\SystemController@index')->name('system');
        Route::get('system/tambah', 'App\Http\Controllers\SystemController@tambah')->name('system.tambah');
        Route::post('system/simpan', 'App\Http\Controllers\SystemController@simpan')->name('system.simpan');
        Route::post('system/update', 'App\Http\Controllers\SystemController@update')->name('system.update');
        Route::get('system/edit/{id}', 'App\Http\Controllers\SystemController@edit')->name('system.edit');
        Route::get('system/hapus/{id}', 'App\Http\Controllers\SystemController@hapus')->name('system.hapus');

        Route::get('jabatan', 'App\Http\Controllers\JabatanController@index')->name('jabatan');
        Route::get('jabatan/tambah', 'App\Http\Controllers\JabatanController@tambah')->name('jabatan.tambah');
        Route::post('jabatan/simpan', 'App\Http\Controllers\JabatanController@simpan')->name('jabatan.simpan');
        Route::post('jabatan/update', 'App\Http\Controllers\JabatanController@update')->name('jabatan.update');
        Route::get('jabatan/edit/{id}', 'App\Http\Controllers\JabatanController@edit')->name('jabatan.edit');
        Route::get('jabatan/hapus/{id}', 'App\Http\Controllers\JabatanController@hapus')->name('jabatan.hapus');

        Route::get('karyawan', 'App\Http\Controllers\KaryawanController@index')->name('karyawan');
        Route::get('karyawan/tambah', 'App\Http\Controllers\KaryawanController@tambah')->name('karyawan.tambah');
        Route::post('karyawan/simpan', 'App\Http\Controllers\KaryawanController@simpan')->name('karyawan.simpan');
        Route::post('karyawan/update', 'App\Http\Controllers\KaryawanController@update')->name('karyawan.update');
        Route::get('karyawan/edit/{id}', 'App\Http\Controllers\KaryawanController@edit')->name('karyawan.edit');
        Route::get('karyawan/hapus/{id}', 'App\Http\Controllers\KaryawanController@hapus')->name('karyawan.hapus');
    });
});

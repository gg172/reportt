-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Mar 2023 pada 15.47
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `report`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `password` text NOT NULL,
  `foto` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `nama`, `username`, `jabatan`, `password`, `foto`, `created_at`, `updated_at`) VALUES
(5, 'admin', 'admin', 1, '$2y$10$uaERAF9g0UNPV2XjyIMB4ud9zUDBgHh5tsKmIib1MLwiZTa1zvMsu', 'Frame 2.png', '2023-02-23 18:41:37', '2023-03-20 19:40:48'),
(6, 'admin2', 'admin2', 1, '$2y$10$ZxdSkx2fQpbLnKoRIndUHe9bfII4Ar3TfeQ45IUMxuN83yyJB3tpi', 'Screenshot_20230206_095929.png', '2023-02-23 23:57:47', '2023-02-23 23:57:47'),
(7, 'admin3', 'admin3', 3, '$2y$10$k21vuCjRYuKaCvY2FczjquSXCcW7Q81V7oPR0D3ZDWOM5ANA3/UXi', 'kk.png', '2023-02-24 01:03:19', '2023-03-10 00:59:14'),
(10, 'adminn', 'adminn', 2, '$2y$10$tefPTkcjr0HZNCaeVq/YNuLmA9rQfkZmi9ml2eW54IW81aHsPOgKW', 'kk.png', '2023-03-26 06:33:10', '2023-03-26 06:33:20');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
